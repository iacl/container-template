# Template for Docker/Singularity/Apptainer Containerization
Projects in our group map files on disk to files on disk. There is always a set of input files and a set of output files for any node in our pipeline. As such, the purpose of any container for our group's projects is to provide a simple command-line interface (CLI) which takes at least a set of input file paths and a set of output file paths and perfors all computation internally.

This repo serves as a template to containerize your code in this way. Instructions follow below.

# Necessary files and directory hierarchy
- `.gitlab-ci.yml`: This file should be copied as-is. The purpose of this file is to tell GitLab to automatically begin building the Docker container on any update to the `main` branch or new tag. It will read the instructions from `Dockerfile`.
- `Dockerfile`: This file must be edited by you. This contains the instructions to install an operating system as well as any dependencies you require for your code to run. Examples of this can be seen at the [SMORE repo](https://gitlab.com/iacl/smore/-/blob/main/Dockerfile?ref_type=heads), [ESPRESO repo](https://gitlab.com/iacl/espreso/-/blob/main/Dockerfile?ref_type=heads), and [ANTs registration repo](https://gitlab.com/iacl/pipeline/registration/-/blob/main/Dockerfile?ref_type=heads).
- `setup.py`: If your code is Python-based, this is necessary to enable `pip install .`. This must be edited by you. Key fields to attend to are `entry_points`, which designates how CLI commands map to your source code and `install_requires`, which is a list of your required Python libraries to install (similar to `requirements.txt`).
- `README.md`: This file must be edited by you. It includes instructions on how to run / install your code, and should also link to your relevant citations for others to use.
- `CHANGELOG.md`: This file must be edited by you. As you fix bugs or add features to your code, you should document it here. We follow [Semantic Versioning](semver.org) as our versioning convention.
- `your-project-name/`: This is a directory which contains your project. It should have the same name as your repo by convention. All your source code should be here.
- `your-project-name/__init__.py`: This file is copied as-is. It imports the your software version from the Git tag automatically.
- `your-project-name/_static_version.py`: This file is copied as-is. It determines your software version from the Git tag automatically.
- `your-project-name/_version.py`: This file is copied as-is. It determines your software version from the Git tag automatically.

# Instructions
1. Clone this repo
2. Remove the `.git/` folder.
3. Replace `your-project-name/` with your source code, ensuring you also bring over `__init__.py`, `_static_version.py`, and `_version.py`.
4. Update `setup.py` to have all your Python requirements.
5. Update `Dockerfile` to include the right install information for your project.
6. Create a new GitLab project and push all the files.
7. Tag your project using [Semantic Versioning](semver.org). `vX.Y.Z` is our current convention.
8. Wait for the project to build: you can see this at `Build > Pipelines > All` on GitLab. You can also see the progress here and correct bugs as they come up.
9. View your tag's url by going to `Deploy > Container Registry > your-project-name`, then click the icon for `Copy image path`. Keep this somewhere.
10. On your server, run `singularity pull docker://your-copied-image-path`, where `your-copied-image-path` is the result from the previous step.
11. Once this completes, you should have a new `.sif` file in the directory you called the previous command. It is now runnable.
